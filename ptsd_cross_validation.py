# Sohini Banerjee

# Sources
"""
https://www.programcreek.com/python/example/87183/sklearn.metrics.confusion_matrix
https://www.programcreek.com/python/example/87183/sklearn.metrics.confusion_matrix
https://www.dataquest.io/blog/python-datetime-tutorial/
https://pyldavis.readthedocs.io/en/latest/modules/API.html
https://developers.arcgis.com/python/api-reference/arcgis.learn.toc.html
https://scikit-optimize.github.io/stable/auto_examples/plots/partial-dependence-plot-2D.html
https://www.freecodecamp.org/news/if-name-main-python-example/
https://stackoverflow.com/questions/47539524/difference-between-np-random-seed1-and-np-random-seed0
https://codereview.stackexchange.com/questions/259015/tic-tac-toe-in-c-language
https://github.com/zhujiagang/DTPP/blob/master/eval_scores_rgb_flow.py
https://stackoverflow.com/questions/66730022/accuracy-difference-in-deep-learning-epochs-and-final-accuracy
https://datascience.stackexchange.com/questions/28493/confusion-matrix-get-items-fp-fn-tp-tn-python/28502
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFE.html
https://towardsdatascience.com/feature-selection-with-pandas-e3690ad8504b
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFE.html
https://scikit-optimize.github.io/stable/modules/generated/skopt.utils.use_named_args.html
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.iloc.html
https://pysindy.readthedocs.io/en/latest/_modules/pysindy/optimizers/stlsq.html
https://www.tensorflow.org/guide/keras/train_and_evaluate
https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html
https://scikit-learn.org/stable/modules/permutation_importance.html
https://heartbeat.fritz.ai/introduction-to-machine-learning-model-evaluation-fa859e1b2d7f
https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.KFold.html
https://stackoverflow.com/questions/41792188/what-does-layer-get-weights-return
https://scikit-learn.org/stable/modules/cross_validation.html
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.iloc.html
https://github.com/zhpmatrix/cged_tf/blob/master/test.py
https://keras.io/api/models/model_training_apis/
https://machinelearningmastery.com/evaluate-performance-deep-learning-models-keras/
https://chelseatroy.com/2019/04/09/applied-data-science-case-study-part-5-fitting-models/
https://faroit.com/keras-docs/1.2.2/models/about-keras-models/
https://stackoverflow.com/questions/64680195/calculate-the-accuracy-of-a-machine-learning-model-without-sklearn
https://het.as.utexas.edu/HET/Software/Numpy/reference/generated/numpy.var.html
https://keras.io/api/models/model_training_apis/
https://stackoverflow.com/questions/57751417/what-is-meant-by-sequential-model-in-keras
https://machinelearningmastery.com/dropout-regularization-deep-learning-models-keras/
https://keras.io/api/optimizers/
https://keras.io/guides/training_with_built_in_methods/
https://vimsky.com/zh-tw/examples/detail/python-method-sklearn.datasets.html
https://www.programcreek.com/python/example/119609/tensorflow.keras.backend.get_session
https://towardsdatascience.com/feature-selection-techniques-for-classification-and-python-tips-for-their-application-10c0ddd7918b
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.GenericUnivariateSelect.html
https://stats.stackexchange.com/questions/420918/how-are-the-scores-computed-with-selectkbest-sklearn
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SelectFromModel.html
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SelectKBest.html#sklearn.feature_selection.SelectKBest.get_support
https://keras.io/api/callbacks/early_stopping/
https://scikit-learn.org/stable/search.html?q=train_test_split
https://stackoverflow.com/questions/15943769/how-do-i-get-the-row-count-of-a-pandas-dataframe
https://stackoverflow.com/questions/37845884/pandas-setting-column-subset-slow
https://stackoverflow.com/questions/21057621/sklearn-labelencoder-with-never-seen-before-values
https://stackoverflow.com/questions/48074462/scikit-learn-onehotencoder-fit-and-transform-error-valueerror-x-has-different
https://scikit-learn.org/stable/modules/generated/sklearn.impute.SimpleImputer.html
https://datascience.stackexchange.com/questions/51890/how-to-use-simpleimputer-class-to-replace-missing-values-with-mean-values-using
https://datascience.stackexchange.com/questions/51890/how-to-use-simpleimputer-class-to-replace-missing-values-with-mean-values-using
https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.QuantileTransformer.html
https://datascience.stackexchange.com/questions/10773/how-does-selectkbest-work
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SelectFromModel.html
https://www.kite.com/python/answers/how-to-use-range(len())-in-python
https://hub.packtpub.com/4-ways-implement-feature-selection-python-machine-learning/
https://stackoverflow.com/questions/39839112/the-easiest-way-for-getting-feature-names-after-running-selectkbest-in-scikit-le
https://scikit-multiflow.readthedocs.io/en/stable/api/generated/skmultiflow.data.MIXEDGenerator.html
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SelectKBest.html
https://www.programcreek.com/python/example/112821/sklearn.feature_selection.mutual_info_classif
"""


import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from keras.models import Sequential, Model
from keras.layers import Activation, Dense, Dropout, Input
from keras.utils import to_categorical
from keras import optimizers, regularizers
from sklearn.metrics import confusion_matrix,accuracy_score, roc_curve, auc
from sklearn.metrics import plot_confusion_matrix
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder, MinMaxScaler, StandardScaler, QuantileTransformer
from pandas import read_csv
from sklearn.impute import SimpleImputer
from sklearn.feature_selection import SelectKBest, f_classif, chi2, mutual_info_classif
from sklearn.model_selection import KFold
import datetime
import skopt
from skopt.space import Integer
from skopt.utils import use_named_args
from skopt import gp_minimize
from tensorflow.keras.callbacks import EarlyStopping
import eli5
from eli5.sklearn import PermutationImportance


#----- Define HyperParameters -----
class HyperParameters:
	# Define Hyper-Parameters
	# Feature Selection - Fraction of total number of features selected for modeling
	# Classification Function - change it in SelectKBest
	classification_function = 'f_classif'
	# classification_function = 'mutual_info_classif'	
	# The inout data is already filtered through t-test and hence only 10% of the features are removed through ANOVA
	feature_select_factor = 0.25
	# Unsupervised Model (autoencoding) Parameters
	# Choose size of our encoded representations (we will reduce our initial features to this number)
	reduced_dim_factor = 0.25
	
	# Supervised Model Parameters
	# Number of layers except the first and the last layer.
	number_of_internal_layers = 2
	# Fraction of total number of selected features in the internal layer
	nodes_per_layer_factor = 2
	# Drop the nodes in the neural network that are less useful
	# drop_out_rate = 0.15
	drop_out_rate = 0.2
	internal_activation_function = 'relu'
	final_activation_function = 'sigmoid'
	number_of_epoch = 200
	loss_function = 'binary_crossentropy'
	optimizer_function='adam'
	batch_size = 32
	learning_rate = 0.01

class PreparedData:
	datafile_name = ''
	processing_time = ''
	dev_x_df = ''
	dev_y = ''
	val_x_df = ''
	val_y = ''


class SelectedFeatures:
	seleccted_cols_x = ''
	selected_x_df = ''

class BestResult:
	hyperParameters = ''
	model = ''
	cols_x = ''
	accuracy = ''

# define the space of hyperparameters to search
hp_search_space = [
#	skopt.space.Categorical(name="classification_function", categories=['f_classif', 'mutual_info_classif']),
#	skopt.space.Real(name="feature_select_factor", low=0.1, high=0.5),
#	skopt.space.Real(name="feature_select_factor", low=0.1, high=1.0),
#	skopt.space.Real(name="reduced_dim_factor", low=0.1, high=0.25),
#	skopt.space.Integer(name="number_of_internal_layers", low=1, high=5),
	skopt.space.Integer(name="number_of_internal_layers", low=1, high=3),
#	skopt.space.Real(name="nodes_per_layer_factor", low=0.5, high=1.5),
#	skopt.space.Real(name="nodes_per_layer_factor", low=0.1, high=0.5),
	skopt.space.Real(name="nodes_per_layer_factor", low=0.1, high=1.0),
#	skopt.space.Real(name="drop_out_rate", low=0.15, high=0.3),
	skopt.space.Real(name="drop_out_rate", low=0.15, high=0.5),
	skopt.space.Categorical(name="internal_activation_function", categories=['relu', 'swish']),
	skopt.space.Categorical(name="final_activation_function", categories=['sigmoid']),
	skopt.space.Integer(name="number_of_epoch", low=25, high=100),
	skopt.space.Integer(name="batch_size", low=16, high=64),
	skopt.space.Real(name="learning_rate", low=0.001, high=0.1)
#	skopt.space.Real(name="learning_rate", low=0.001, high=0.05)
#	skopt.space.Real(name="learning_rate", low=0.01, high=0.1)
]

prepared_data = PreparedData()
selected_features = SelectedFeatures()

early_stopping = EarlyStopping(patience=10)

	
def swish(x, beta = 1): 
    return (x * sigmoid(beta * x))
      
def prepare_data(datafile_name, processing_time):
	# Read data
#	path = '/Users/nbanerjee/Desktop/Sohini/Science_Fair/Data/' + datafile_name
#	path = '/Users/nbanerjee/Desktop/Sohini/Original_Data/' + datafile_name

	"""		
	#path = 'data/'+datafile_name
	path = 'data/Final Original and Validation/'+'Final-Original-'+datafile_name
	all_df = read_csv(path, header = 0)
	
	row_count = all_df.shape[0]  # gives number of row count
	col_count = all_df.shape[1]  # gives number of col count
	print('All Row Count %d, Column Count %d' % (row_count, col_count))

	# Split in 70% dev and 30% val
	dev_df, val_df = train_test_split(all_df, test_size = 0.30, random_state = 100)
	"""	
	"""
	# Split between train and test is done outside of this program
	dev_count = int(0.70 * row_count)
	dev_df = all_df[0:dev_count-1]
	val_df = all_df[dev_count:]
	"""
	
	path = 'data/Final Original and Validation/'+'Final-Original-'+datafile_name
	dev_df = read_csv(path, header = 0)
	path = 'data/Final Original and Validation/'+'Final-Validation-'+datafile_name
	val_df = read_csv(path, header = 0)
	

	row_count = dev_df.shape[0]  # gives number of row count
	col_count = dev_df.shape[1]  # gives number of col count
	print('Dev Row Count %d, Column Count %d' % (row_count, col_count))

	row_count = val_df.shape[0]  # gives number of row count
	col_count = val_df.shape[1]  # gives number of col count
	print('Val Row Count %d, Column Count %d' % (row_count, col_count))

	# Split dataframe into input and output columns
	dev_x_df, dev_y_df = dev_df.iloc[:, :-1], dev_df.iloc[:, -1]
	val_x_df, val_y_df = val_df.iloc[:, :-1], val_df.iloc[:, -1]
	
	print ("dev_x_df before transform: ")
	print (dev_x_df)

	print ("val_x_df before transform: ")
	print (val_x_df)

	# Ensure all data are floating point values
	dev_x_df = dev_x_df.astype('float32')
	val_x_df = val_x_df.astype('float32')
	
	# Encode strings to integer
	print ('Before label encoder dev_y')
	print (dev_y_df.values)
	dev_y = LabelEncoder().fit_transform(dev_y_df.values)
	print ('Label encoder dev_y')
	print (dev_y)
	
	# LabelEncoder().fit_transform(val_y_df)
	print ('Before label encoder val_y')
	print (val_y_df.values)
	val_y = LabelEncoder().fit_transform(val_y_df.values)
	print ('Label encoder val_y')
	print (val_y)
	
	# Determine the number of input features
	n_features = dev_x_df.shape[1]
	print("n_features in dev " + str(n_features))
	n_features = val_x_df.shape[1]
	print("n_features in val " + str(n_features))

	# replace missing data with sklearn

	imputer = SimpleImputer(missing_values=np.nan, strategy='median')

	# transform training data
	imputed_values = imputer.fit_transform(dev_x_df) 
	dev_x_df.loc[:,:] = imputed_values

	# transform final testing data
	imputed_values = imputer.fit_transform(val_x_df) 
	val_x_df.loc[:,:] = imputed_values

	# data normalization with sklearn

	scaler = QuantileTransformer(output_distribution='normal')
	#scaler = MinMaxScaler()
	#scaler = StandardScaler()

	# transform training data
	scaled_values = scaler.fit_transform(dev_x_df) 
	dev_x_df.loc[:,:] = scaled_values

	# transform final testing data
	scaled_values = scaler.fit_transform(val_x_df) 
	val_x_df.loc[:,:] = scaled_values
	
	print ("dev_x_df after transform: ")
	print (dev_x_df)

	print ("val_x_df after transform: ")
	print (val_x_df)

	prepared_data.datafile_name = datafile_name
	prepared_data.processing_time = processing_time
	prepared_data.dev_x_df = dev_x_df
	prepared_data.dev_y = dev_y
	prepared_data.val_x_df = val_x_df
	prepared_data.val_y = val_y
	return

def select_features(datafile_name, processing_time):
	#-----Build the feature subset using classification function -----
	# Determine the number of input features
	print ("Selecting features: " + datafile_name)
	dev_x_df = prepared_data.dev_x_df
	dev_y = prepared_data.dev_y
	n_features = dev_x_df.shape[1]
	# Get the feature names
	feature_names = dev_x_df.columns.values
	# Choose size of selected features
	selected_dim_size = 'all'
	print("Selected Dim Size " + str(selected_dim_size))
	
	# Create and fit selector 
	# Use ANOVA f-test using f_classif or mutual_info_classif
	score_func = f_classif
	# score_func = mutual_info_classif
	selector = SelectKBest(score_func, k = selected_dim_size)
	selector.fit(dev_x_df, dev_y)
	
	# Get columns to keep and create new dataframe with those only
	cols_all_x = selector.get_support(indices=True)
	
	# what are scores for the features
	cols_x = []
	for i in range(len(selector.scores_)):
		j = cols_all_x[i]
		print('Feature-%d %s %f %f' % (j, feature_names[j], selector.scores_[i], selector.pvalues_[i]))
		if (selector.pvalues_[i] <= 0.05):
			cols_x.append(cols_all_x[i])

	# Get the dataframe for the selected columns
	selected_dev_x_df = dev_x_df.iloc[:,cols_x]
	selected_column_names = selected_dev_x_df.columns.values
	print(selected_column_names)
	# what are the best scores for the features	
	for i in range(len(cols_x)):
		print(' Result, %s, %s, Selected Feature, %d, %s, %f %f' % (datafile_name, processing_time, 
				cols_x[i], selected_column_names[i], selector.scores_[cols_x[i]], selector.pvalues_[cols_x[i]]))	

	
	# Display the number of best input features
	n_features_selected = selected_dev_x_df.shape[1]
	print("Best n_features in train " + str(n_features_selected))
	selected_features.selected_dev_x_df = selected_dev_x_df
	selected_features.selected_cols_x = cols_x
	
	return
			
def run_feature_selection(hp, datafile_name, processing_time, train_x_df, train_y):
	#-----Build the feature subset using classification function -----
	# Determine the number of input features
	n_features = train_x_df.shape[1]
	# Get the feature names
	feature_names = train_x_df.columns.values
	# Choose size of selected features
	selected_dim_size = 'all'
	#selected_dim_size = max(2, int(n_features * hp.feature_select_factor))
	print("Selected Dim Size " + str(selected_dim_size))
	
	# Create and fit selector 
	# Use ANOVA f-test using f_classif or mutual_info_classif
	if (hp.classification_function == 'f_classif'):
		score_func = f_classif
	if (hp.classification_function == 'mutual_info_classif'):
		score_func = mutual_info_classif
	selector = SelectKBest(score_func, k = selected_dim_size)
	# selector = SelectKBest(mutual_info_classif, k = 10)
	selector.fit(train_x_df, train_y)
	
	# Get columns to keep and create new dataframe with those only
	cols_all_x = selector.get_support(indices=True)
	
	# what are scores for the features
	cols_x = []
	for i in range(len(selector.scores_)):
		print('Feature-%d %s %f %f' % (i, feature_names[i], selector.scores_[i], selector.pvalues_[i]))
		if (selector.pvalues_[i] <= 0.1):
			cols_x.append(cols_all_x[i])

	# Get the dataframe for the selected columns
	selected_train_x_df = train_x_df.iloc[:,cols_x]
	selected_column_names = selected_train_x_df.columns.values
	print(selected_column_names)
	# what are the best scores for the features	
	for i in range(len(cols_x)):
		print(' Result, %s, %s, Selected Features, %d, %s, %f %f' % (datafile_name, processing_time, 
				cols_x[i], selected_column_names[i], selector.scores_[cols_x[i]], selector.pvalues_[cols_x[i]]))	

	
	# Display the number of best input features
	n_features_selected = selected_train_x_df.shape[1]
	print("Best n_features in train " + str(n_features_selected))
	
	return selected_train_x_df, cols_x

#----- Define supervised model using selected features -----
def define_supervised_model(hp, datafile_name, processing_time, n_features):
	
	print("Supervised Model - Number of features: " + str(n_features))
	
	# Define model
	model = Sequential()
	model.add(Dense(n_features, activation = hp.internal_activation_function, 
				kernel_initializer = 'he_normal', input_shape = (n_features,)))
	model.add(Dropout(hp.drop_out_rate))
	for i in range(hp.number_of_internal_layers):
		print ('Layer %d' %i)
		model.add(Dense(n_features * hp.nodes_per_layer_factor, 
						activation = hp.internal_activation_function, 
						kernel_initializer = 'he_normal'))
		model.add(Dropout(hp.drop_out_rate))
	model.add(Dense(1, activation = hp.final_activation_function))
	
	# Compile the model

	optimizer = optimizers.Adam(learning_rate=hp.learning_rate)
	model.compile(loss=hp.loss_function, optimizer=optimizer, 
					metrics=['accuracy'])
	
	return model

#----- Build and evaluate supervised model using prepared data -----
def train_supervised_model(hp, datafile_name, processing_time, model, train_x_df, train_y):
				
	# Fit the model
	history = model.fit(train_x_df, train_y, validation_split=0.2, 
						epochs=hp.number_of_epoch, batch_size=hp.batch_size, verbose = 0)
#callbacks=[early_stopping])
	
	return model
	
def eval_supervised_model(hp, datafile_name, processing_time, model, dev_x_df, dev_y):
	
	n_features = dev_x_df.shape[1]
	print("Supervised Model - Number of dev features: " + str(n_features))

	# Perform cross validation
	kf = KFold(n_splits = 5, shuffle = True)
	loss = 0
	accuracy_mean = 0
	accuracies = []
	for i in range(5):
		weights = model.get_weights()
		result = next(kf.split(dev_x_df), None)
		print('KFold train: %s, KFold test: %s' % (result[0], result[1]))
		train_x_df = dev_x_df.iloc[result[0]]
		test_x_df = dev_x_df.iloc[result[1]]
		train_y = dev_y[result[0]]
		test_y = dev_y[result[1]]

		print('CV train_x_df:')
		print(train_x_df)
		print('CV train_y:')
		print(train_y)

		print('CV test_x_df:')
		print(test_x_df)
		print('CV test_y:')
		print(test_y)

		model.fit(train_x_df, train_y, epochs=hp.number_of_epoch, batch_size=hp.batch_size, verbose = 0)
		#callbacks=[early_stopping])
		score = model.evaluate(test_x_df, test_y, verbose = 0)
		loss = loss + score[0]
		accuracy = score[1]
		accuracies.append(accuracy)
		model.set_weights(weights)
	loss = loss/5
	accuracy_mean = np.mean(accuracies)
	variance = np.var(accuracies)

	print("Supervised Model - CV Loss Mean: %.3f" % loss)
	print("Supervised Model - CV Accuracy Mean: %.3f" % accuracy_mean)
	print("Supervised Model - CV Accuracy Variance: %.3f" % variance)

	model.fit(dev_x_df, dev_y, epochs=hp.number_of_epoch, batch_size=hp.batch_size, verbose = 0)
	return model, loss, accuracy, variance

def val_supervised_model(datafile_name, processing_time, trained_model, val_x_df, val_y, cols_x):
	
	selected_val_x_df = val_x_df.iloc[:,cols_x]
	#selected_val_x_df = val_x_df
	selected_column_names = selected_val_x_df.columns.values
	print(selected_column_names)
	
	n_features_selected = selected_val_x_df.shape[1]
	print("Best n_features in final test " + str(n_features_selected))

	# Evaluate the model
	score = trained_model.evaluate(selected_val_x_df, val_y, verbose = 0)

	print("Supervised Model - Final Test Loss: %.3f" % score[0])
	print("Supervised Model - Final Test Accuracy: %.3f" % score[1])
	
	accuracy = score[1]
	# Generate confusion matrix
	threshold = 0.5
	pred_y = trained_model.predict(selected_val_x_df, verbose = 2)
	print ('pred y ')
	print (pred_y)
	print ('val y ')
	print (val_y)
	for i in range(len(pred_y)):
		pred_y[i] = (pred_y[i] > threshold)
	print ('pred y ')
	print (pred_y)
	# Compute confusion matrix
	cnf_matrix = confusion_matrix(val_y, pred_y).ravel()
	tn, fp, fn, tp = confusion_matrix(val_y, pred_y).ravel()

	print ("Final Test, DataFile Name , Timestamp, Final Test, Test Accuracy, TN, FP, FN, TP")	

	print('Final Test, %s, %s, Final Test, %.3f, %.3f, %.3f, %.3f, %.3f' % (datafile_name, processing_time, 
				accuracy, tn, fp, fn, tp))
	
	# Explain the model
	print('Result, %s, %s, Important Feature,' % (datafile_name, processing_time))
	if (n_features_selected > 0):
		perm = PermutationImportance(trained_model, scoring='neg_mean_squared_error', random_state=1).fit(selected_val_x_df, val_y)
		print(eli5.format_as_text(eli5.explain_weights(perm, feature_names = selected_val_x_df.columns.tolist())))

	return score, cnf_matrix


@skopt.utils.use_named_args(hp_search_space)
def eval_model_objective_function(**params):	

	# Initialize hyperparameters
	hp = HyperParameters()
	#hp.classification_function = params["classification_function"]
	#hp.feature_select_factor = params["feature_select_factor"]
	#hp.reduced_dim_factor = params["reduced_dim_factor"]
	hp.number_of_internal_layers = params["number_of_internal_layers"]
	hp.nodes_per_layer_factor = params["nodes_per_layer_factor"]
	hp.drop_out_rate = params["drop_out_rate"]
	hp.internal_activation_function = params["internal_activation_function"]
	hp.final_activation_function = params["final_activation_function"]
	hp.number_of_epoch = params["number_of_epoch"]
	hp.batch_size = params["batch_size"]
	hp.learning_rate = params["learning_rate"]
	
	datafile_name = prepared_data.datafile_name
	# processing_time = prepared_data.processing_time
	processing_time = datetime.datetime.now()
	dev_x_df = prepared_data.dev_x_df
	dev_y	= prepared_data.dev_y
	val_x_df = prepared_data.val_x_df
	val_y = prepared_data.val_y

	print ("Prepared data: ")
	print (prepared_data.dev_x_df)
	n_features = dev_x_df.shape[1]
	n_features_selected = -1
	n_features_reduced_dim = -1
	selected_dev_x_df = ''
	model_type = 'Supervised-CV-Bayes'
		

	##### Feature Selection -> Supervised
	model_type = 'Feature Selection-Supervised'
	selected_cols_x = selected_features.selected_cols_x
	selected_dev_x_df = selected_features.selected_dev_x_df
	
	"""
	if (cols_selected_features == ''):					
		# Run feature selection 
		print ("Selecting features: " + datafile_name)
		selected_dev_x_df, cols_selected_features = run_feature_selection(hp, 
				datafile_name, processing_time, dev_x_df, dev_y)
	"""

	n_features_selected = selected_dev_x_df.shape[1]

	"""
	### Next 2 lines if feature selection is disabled, comment them when feature selection above is enabled
	selected_dev_x_df = dev_x_df
	n_features_selected = n_features
	"""

	print ("Running supervised model: " + datafile_name)
	# Define supervised model
	model = define_supervised_model(hp, datafile_name, processing_time, n_features_selected)
				
	# Run supervised model
	model, loss, accuracy, variance = eval_supervised_model(hp, datafile_name, processing_time,
			model, selected_dev_x_df, dev_y)
							
	tn, fp, fn, tp = 0, 0, 0, 0
	minimizing_score = 1 - accuracy
			

	# Test on validation dataset but do not include in bayesian decision
	final_score, final_cnf_matrix = val_supervised_model(datafile_name, processing_time, model, val_x_df, val_y, selected_cols_x)
	final_loss = final_score[0]
	final_accuracy = final_score[1]
	accuracy_difference = abs(final_accuracy - accuracy)
	final_tn, final_fp, final_fn, final_tp = final_cnf_matrix.ravel()

	print ("Result, %s, %s, %s, %d, %d, %d, %.3f, %.3f, %.3f, %s, %.3f, %.3f, %d, %.3f, %.3f, %s, %s, %d, %d, %.3f, %d, %d, %d, %d, %.3f, %.3f, %d, %d, %d, %d, %.3f" 
		%(datafile_name, processing_time, model_type, n_features, n_features_selected, 
		n_features_reduced_dim, loss, accuracy, variance, hp.classification_function, 
		hp.feature_select_factor, hp.reduced_dim_factor, hp.number_of_internal_layers,
		hp.nodes_per_layer_factor, hp.drop_out_rate, hp.internal_activation_function,
		hp.final_activation_function, hp.number_of_epoch, hp.batch_size, hp.learning_rate,tn, fp, fn, tp, final_loss, final_accuracy, final_tn, final_fp, final_fn, final_tp, accuracy_difference))
	
	return minimizing_score
    
def main():
	print("Hello Model!")
	print ("Result, DataFile Name , Timestamp, Model Type, Original Features Count, Selected Features Count, Reduced Features Count, Test Loss, Test Accuracy, Accuracy Variance, Classification Function, Feature Select Factor, Reduced Dim Factor, Number of Internal Layers, Nodes Per Layer Factor, Drop Out Rate, Internal Activation Function, Final Activation Function, Number of Epoch, Batch Size, Learning Rate, Confusion Matrix TN, Confusion Matrix FP, Confusion Matrix FN, Confusion Matrix TP, Final Test Loss, Final Test Accuracy, Final Confusion Matrix TN, Final Confusion Matrix FP, Final Confusion Matrix FN, Final Confusion Matrix TP, Accuracy Difference")
	print ("Result, DataFile Name , Timestamp, Feature, Column Id, Column Name, ANOVA Score, p-Value")	

	# Initialize random number seed
	np.random.seed(1)
	
	# List datafiles
	datafiles = [
#		"Male_Methylation-ZymoProbe_20170216.csv", 
#		"Male_Methylation-ZymoGene_20170216.csv", 
#		"Male_Methylation-Illumina_20160620.csv", 
#		"Male_Methylation-GR_20170228.csv",
#		"modified-Male-Original-Methylation-Illumina.csv"
#		"original-Male-Original-Methylation-Illumina.csv"
#		"modified-Male-Original-Clinical-BioABMNYU.csv",  (too small, no data)
#		"modified-Male-Original-Clinical-Lab.csv",   
#		"modified-Male-Original-Endocrine-Blood.csv", (too few features)
#		"modified-Male-Original-Methylation-ZymoGene.csv", 
#		"modified-Male-Original-miRNA-Deplete.csv", 
#		"modified-Male-Original-Methylation-ZymoProbe.csv", 
#		"modified-Male-Original-miRNA-Exosome.csv", 
#		"modified-Male-Original-mRNA.csv", 
#		"modified-Male-Original-Metabolomics-Metabolon.csv", 
#		"modified-Male-Original-miRNA-Plasma.csv", 
#		"modified-Male-Original-Metabolomics-UCSF.csv"
#		"modified-Male-Original-Protein-ELISA.csv" (too few features)
#		"updated3-Male-Original-miRNA-Exosome.csv",
#		"updated3-Male-Original-miRNA-Plasma.csv",
#		"updated3-Male-Original-Metabolomics-Metabolon.csv",
#		"updated3-Male-Original-Methylation-ZymoGene.csv",
#		"updated3-Male-Original-Methylation-ZymoProbe.csv"
#		"updated3-Male-Original-mRNA.csv",
		"Male-Endocrine-Blood.csv",
		"Male-Endocrine-Urine.csv",			
		"Male-Metabolomics-UCSF.csv",
		"Male-Methylation-GR.csv",
		"Male-Methylation-ZymoGene.csv",
		"Male-Methylation-ZymoProbe.csv",
		"Male-miRNA-Deplete.csv",
		"Male-miRNA-Exosome.csv",
		"Male-miRNA-Plasma.csv",
#		"Male-OLink.csv",
		"Male-Protein-ELISA.csv",
		"Male-Protein-SRM.csv"
				]
		
	for datafile_name in datafiles:
		
		print ("Procesing file: " + datafile_name)
		processing_time = datetime.datetime.now()
		prepared_data = PreparedData()
		selected_features = SelectedFeatures()

		# Prepare data
		print ("Preparing data: " + datafile_name)
		# Track prepared data for use within the objective function
		prepare_data (datafile_name, processing_time)
		# Track selected features for use within the objective function
		select_features (datafile_name, processing_time)
				
		# perform optimization
		search_result = gp_minimize(eval_model_objective_function, hp_search_space)
		# summarizing finding:
		print('Best Hyper Parameters: ' + datafile_name)
		print(search_result.x)	
		print('Function Value at Minimum:')
		print(search_result.fun)

		
if __name__ == "__main__":
    main()